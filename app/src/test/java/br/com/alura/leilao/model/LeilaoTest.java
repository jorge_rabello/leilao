package br.com.alura.leilao.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LeilaoTest {

  @Test
  public void getDescricao() {
    // given
    Leilao console = new Leilao("Console");

    // when
    String descricaoDevolvida = console.getDescricao();

    // then
    assertEquals("Console", descricaoDevolvida);
  }

  @Test
  public void getMaiorLance() {
    // given
    Leilao console = new Leilao("Console");
    console.propoe(new Lance(new Usuario("Jorge"), 200.0));

    // when
    double maiorLanceDevolvido = console.getMaiorLance();

    // then
    assertEquals(200.0, maiorLanceDevolvido, 0.0001);
  }

}
